#import <Foundation/Foundation.h>
#import "Contact.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Contact* julien = [[[Contact alloc] initWithName:@"Julien Ponge"
                                                  email:@"julien.ponge@insa-lyon.fr"
                                                    age:[NSNumber numberWithInt:31]] autorelease];
        Contact* mrbean = [[[Contact alloc] initWithName:@"Mr Bean"
                                                   email:@"bean@gmail.com"
                                                     age:[NSNumber numberWithInt:58]] autorelease];
        NSLog(@"%@", julien);
        NSLog(@"%@", mrbean);
        
        NSString* path = [NSHomeDirectory() stringByAppendingPathComponent:@"Desktop/awesome-data"];
        NSLog(@"Path = %@", path);
        
        NSMutableData* data = [[NSMutableData alloc] init];
        NSKeyedArchiver* archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver setOutputFormat:NSPropertyListXMLFormat_v1_0];
        [archiver encodeObject:[NSSet setWithObjects:julien, mrbean, nil]];
        [archiver finishEncoding];
        if ([data writeToFile:path atomically:YES] == NO) {
            NSLog(@"WTF?");
        }
        [data release];
        [archiver release];
        
        NSData* readData = [[NSData alloc] initWithContentsOfFile:path];
        NSKeyedUnarchiver* unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:readData];
        NSSet* collection = [unarchiver decodeObject];
        [readData release];
        [unarchiver finishDecoding];
        [unarchiver release];
        
        for (Contact* contact in collection) {
            NSLog(@">>> %@", contact);
        }
    }
    return 0;
}

