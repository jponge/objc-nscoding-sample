#import "Contact.h"

@implementation Contact

-(id) initWithName:(NSString*)iname email:(NSString*)iemail age:(NSNumber*)iage {
    self = [super init];
    if (self) {
        self.name = iname;
        self.email = iemail;
        self.age = iage;
    }
    return self;
}

@synthesize name;
@synthesize email;
@synthesize age;

-(NSString*) description {
    return [NSString stringWithFormat:@"%@ <%@> aged %@", name, email, age];
}

-(void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:name forKey:@"name"];
    [encoder encodeObject:email forKey:@"email"];
    [encoder encodeObject:age forKey:@"age"];
}

-(id) initWithCoder:(NSCoder *)decoder {
    NSString* dname = [decoder decodeObjectForKey:@"name"];
    NSString* demail = [decoder decodeObjectForKey:@"email"];
    NSNumber* dage = [decoder decodeObjectForKey:@"age"];
    return [self initWithName:dname email:demail age:dage];
}

@end
