#import <Foundation/Foundation.h>

@interface Contact : NSObject <NSCoding> {
    NSString* _name;
    NSString* _email;
    NSNumber* _age;
}

-(id) initWithName:(NSString*)iname email:(NSString*)iemail age:(NSNumber*)iage;

@property(nonatomic, retain) NSString* name;
@property(nonatomic, retain) NSString* email;
@property(nonatomic, retain) NSNumber* age;

@end
